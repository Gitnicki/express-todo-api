const express = require("express");
const app = express();
const PORT = 3000;

app.use(express.json());

let tasks = [];

app.get("/tasks", (req, res) => {
  res.json(tasks);
});

app.post("/task", (req, res) => {
  const task = req.body;
  tasks.push(task);
  res.status(201).json(task);
});

app.put("/task/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const updatedTask = req.body;
  tasks[id] = updatedTask;
  res.json(updatedTask);
});

app.delete("/task/:id", (req, res) => {
  const id = parseInt(req.params.id);
  tasks.splice(id, 1);
  res.sendStatus(204);
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
